const fs = require('fs')

const { successResponse } = require('../helper/response')

class Info {
    constructor () {
    this.info = []
    const readInfo = fs.readFileSync('./data/data.json', 'utf-8')
    if (readInfo == ""){
        this.parsedInfo = this.info
    } else {
        this.parsedInfo = JSON.parse(readInfo)
    }
    }

    getInfo = (req, res) => {
        successResponse(res, 200, this.parsedInfo, {total: this.parsedInfo.length})
    }

    getDetailedInfo = (req, res) => {
        const index = req.params.index
        successResponse(res, 200, this.parsedInfo[index])
    
    }
    insertInfo = (req, res) => {
        const body = req.body
        const param = {
            "name" : body.name,
            "password": body.password
        }

        this.parsedInfo.push(param)
        fs.writeFileSync('./data/data.json', JSON.stringify(this.parsedInfo))
        successResponse(res, 201, param)

    }

    updateInfo = (req, res) => {
        const index = req.params.index
        const body = req.body
        
        if (this.parsedInfo[index]) {
        this.parsedInfo[index].name = body.name
        this.parsedInfo[index].password = body.password

        fs.writeFileSync("./data/data.json", JSON.stringify(this.parsedInfo))
        successResponse(res, 200, this.parsedInfo[index])
        }else {
            successResponse(res, 422, null)
        }
    }

    deleteInfo = (req, res) => {

        const index = req.params.index
        this.parsedInfo.splice(index, 1)

        successResponse(res, 200, null)
    }
}

module.exports = Info