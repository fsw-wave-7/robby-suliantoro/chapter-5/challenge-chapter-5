const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')

const jsonParser = bodyParser.json()

const Info = require('../controller/info')
const info = new Info

const logger = (req, res, next) => {
    console.log(`LOG: ${req.method} ${req.url}`)
    next()
}

const indexinfo = express.Router()

indexinfo.use(logger)
indexinfo.use(jsonParser)

indexinfo.get("/info", info.getInfo)
indexinfo.get("/info/:index", info.getDetailedInfo)
indexinfo.post("/info", info.insertInfo)
indexinfo.put("/info/:index", info.updateInfo)
indexinfo.delete("/info/:index",info.deleteInfo)


//indexinfo.listen(port, () => {
//    console.log("server tugas challenge 5 jalan")
//})

module.exports = indexinfo