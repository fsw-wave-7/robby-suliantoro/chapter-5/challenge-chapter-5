const express = require('express')
const morgan = require('morgan')
const path = require('path')
const bodyParser = require('body-parser')
const fs = require('fs')

const indexinfo = require('./routes/indexinfo.js')

const app = express()
const port = 4000

// set view engine
app.set('view engine', 'ejs')

//load static files
app.use(express.static(__dirname + '/public'))

//third party middleware for logging
app.use(morgan('dev'))

//load indexinfo routes
app.use('/indexinfo', indexinfo)

app.get('/', function(req, res) {
    res.render(path.join(__dirname, './views/index.ejs'), {
        content: './pages/index3'
    })
})

app.get('/game', function(req, res) {
    res.render(path.join(__dirname, './views/index.ejs'), {
        content: './pages/index4'
    })
})

app.get('/check-log', function(req, res) {
    res.json({
        'check': 'log'
    })
})

//Internal server error middleware
app.use(function(err, req, res, next) {
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
})

// 404 handler middleware
app.use(function(req, res, next) {
    res.status(404).json({
        status: 'fail',
        errors: 'Are you lost?'
    })
})

app.listen(port, () => {
    console.log("server tugas challenge 5 jalan")
})