function getPilihanComputer() {
    const comp = Math.random();
    if ( comp < 0.34) return 'gunting1png';
    if ( comp >= 0.34 && comp < 0.67) return 'kertas1png';
    return 'batu1png';
}

function getHasil(comp, player) {
    if ( player == comp ) return 'SERI!';
    if ( player == 'gunting1png') return ( comp == 'kertas1png') ? 'MENANG!' : 'KALAH!';
    if ( player == 'kertas1png') return ( comp == 'gunting1png') ? 'KALAH!' : 'MENANG!';
    if ( player == 'batu1png') return ( comp == 'kertas1png') ? 'KALAH!' : 'MENANG!'; 
}

function getKode(pilihanComputer, pilihanPlayer, hasil) {
  if ( pilihanPlayer == "gunting1png" && pilihanComputer == "kertas1png" && hasil == 'MENANG!' ) return 'PLAYER 1 WIN PAKE GUNTING';
  if ( pilihanPlayer == "kertas1png" && pilihanComputer == "batu1png" && hasil == 'MENANG!' ) return 'PLAYER 1 WIN PAKE KERTAS';
  if ( pilihanPlayer == "batu1png" && pilihanComputer == "gunting1png" && hasil == 'MENANG!' ) return 'PLAYER 1 WIN PAKE BATU';
  if ( pilihanPlayer == "gunting1png" && pilihanComputer == "gunting1png" && hasil == 'SERI!' ) return 'DRAW PAKE GUNTING';
  if ( pilihanPlayer == "kertas1png" && pilihanComputer == "kertas1png" && hasil == 'SERI!' ) return 'DRAW PAKE KERTAS';
  if ( pilihanPlayer == "batu1png" && pilihanComputer == "batu1png" && hasil == 'SERI!' ) return 'DRAW PAKE BATU';
  if ( pilihanPlayer == "kertas1png" && pilihanComputer == "gunting1png" && hasil == 'KALAH!' ) return 'COMPUTER WIN PAKE GUNTING';
  if ( pilihanPlayer == "batu1png" && pilihanComputer == "kertas1png" && hasil == 'KALAH!' ) return 'COMPUTER WIN PAKE KERTAS';
  if ( pilihanPlayer == "gunting1png" && pilihanComputer == "batu1png" && hasil == 'KALAH!' ) return 'COMPUTER WIN PAKE BATU';
}

const pGunting1 = document.querySelector('.gunting1png');
pGunting1.addEventListener('click', function () {
    const pilihanComputer = getPilihanComputer();
    const pilihanPlayer = pGunting1.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);
    const kode = getKode(pilihanComputer, pilihanPlayer, hasil);

    console.log('comp : ' + pilihanComputer);
    console.log('player : ' + pilihanPlayer);
    //console.log('hasil : ' + hasil);
    console.log('kesimpulan : ' + kode);
    
    if (kode == 'PLAYER 1 WIN PAKE GUNTING') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "PLAYER 1 WIN";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = 'green';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = 'red';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
      }
    else if (kode == 'DRAW PAKE GUNTING') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "DRAW";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = 'yellow';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = 'yellow';
      }
    else if (kode == 'COMPUTER WIN PAKE BATU') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "COM WIN";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = 'red';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = 'green';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
      }
    else {
    document.getElementById("demo2").innerHTML = "";
    document.getElementById("demo").innerHTML = "V S";
    document.getElementById("demo").style.backgroundColor = '#9C835F';
    document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
    }
});

const pKertas1 = document.querySelector('.kertas1png');
pKertas1.addEventListener('click', function () {
    const pilihanComputer = getPilihanComputer();
    const pilihanPlayer = pKertas1.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);
    const kode = getKode(pilihanComputer, pilihanPlayer, hasil);

    console.log('comp : ' + pilihanComputer);
    console.log('player : ' + pilihanPlayer);
    //console.log('hasil : ' + hasil);
    console.log('kesimpulan : ' + kode);

    if (kode == 'PLAYER 1 WIN PAKE KERTAS') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "PLAYER 1 WIN";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = 'green';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = 'red';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
      }
    else if (kode == 'DRAW PAKE KERTAS') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "DRAW";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = 'yellow';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = 'yellow';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
      }
    else if (kode == 'COMPUTER WIN PAKE GUNTING') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "COM WIN";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = 'red';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = 'green';
      }
    else {
    document.getElementById("demo2").innerHTML = "";
    document.getElementById("demo").innerHTML = "V S";
    document.getElementById("demo").style.backgroundColor = '#9C835F';
    document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
    }
});

const pBatu1 = document.querySelector('.batu1png');
pBatu1.addEventListener('click', function () {
    const pilihanComputer = getPilihanComputer();
    const pilihanPlayer = pBatu1.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);
    const kode = getKode(pilihanComputer, pilihanPlayer, hasil);

    console.log('comp : ' + pilihanComputer);
    console.log('player : ' + pilihanPlayer);
    //console.log('hasil : ' + hasil);
    console.log('kesimpulan : ' + kode);

    if (kode == 'PLAYER 1 WIN PAKE BATU') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "PLAYER 1 WIN";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = 'green';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = 'red';
      }
    else if (kode == 'DRAW PAKE BATU') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "DRAW";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = 'yellow';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = 'yellow';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
      }
    else if (kode == 'COMPUTER WIN PAKE KERTAS') {
      document.getElementById("demo2").innerHTML = "";
      document.getElementById("demo").innerHTML = "COM WIN";
      document.getElementById("demo").style.backgroundColor = 'orange';
      document.getElementsByClassName("batu1png")[0].style.backgroundColor = 'red';
      document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
      document.getElementsByClassName("kertas2png")[0].style.backgroundColor = 'green';
      document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
      }
    else {
    document.getElementById("demo2").innerHTML = "";
    document.getElementById("demo").innerHTML = "V S";
    document.getElementById("demo").style.backgroundColor = '#9C835F';
    document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
    }
});

const pRefresh = document.querySelector('.refreshpng');
pRefresh.addEventListener('click', function () {
    document.getElementById("demo2").innerHTML = "";
    document.getElementById("demo").innerHTML = "V S";
    document.getElementById("demo").style.backgroundColor = '#9C835F';
    document.getElementsByClassName("batu1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("kertas1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("gunting1png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("batu2png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("kertas2png")[0].style.backgroundColor = '#9C835F';
    document.getElementsByClassName("gunting2png")[0].style.backgroundColor = '#9C835F';
});